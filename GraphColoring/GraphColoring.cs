﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;

namespace GraphColoring
{
    public partial class GraphColoring : Form
    {
        public GraphColoring()
        {
            InitializeComponent();
            //Cho phép các thread sử dụng chung tài nguyên
            Control.CheckForIllegalCrossThreadCalls = false;   
        }
        //Lưu thông tin các Đồ thị
        List<DoThi> CacDothi = new List<DoThi>();
        int sldt,sl;
        XElement xdoc;
        //Kết quả trả ra của mối Thread
        List<string> LstThread1 = new List<string>();
        List<string> LstThread2 = new List<string>();
        List<string> LstThread3 = new List<string>();
        List<string> LstThread4 = new List<string>();
        //Ds kết quả
        List<string> Resuilt = new List<string>();
        //Form load
        private void GraphColoring_Load(object sender, EventArgs e)
        {
            //txtFilePath.Text = "../../GraphColoring.xml";
            //LoadCbx();
            //xdoc = XElement.Load(txtFilePath.Text);
        }
        //Chọn file
        private void btnInputFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "xml file (*.xml)|*.xml| All file (*.*)|*.*";
            if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFilePath.Text = open.FileName;
            }
            LoadCbx();
            ShowData();

        }
        //Xem Đồ thị
        private void btnShowDT_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFilePath.Text))
            {
                if (cbxTenDT1.Text.Trim() != null)
                {
                    ShowDT(cbxTenDT1.Text.Trim());
                }
                else
                {
                    MessageBox.Show("Nhập tên đồ thị");
                }
            }
            else
            {
                MessageBox.Show("Bạn phải chọn file XML trước khi thực hiện thao tác");
            }
            
        }
        //Close
        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //Clear click
        private void btnClear_Click(object sender, EventArgs e)
        {            
            clear();
        }
        //Thêm đồ thị
        private void btnThemDT_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFilePath.Text))
            {
                ThemDT();
            }
            else
            {
                MessageBox.Show("Bạn phải chọn file XML trước khi thực hiện thao tác");
            }
        }
        //Xóa đồ thị
        private void btnXoaDT_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFilePath.Text))
            {
                XoaDT();
            }
            else
            {
                MessageBox.Show("Bạn phải chọn file XML trước khi thực hiện thao tác");
            }
        }
        //Thêm đỉnh
        private void btnThemD_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFilePath.Text))
            {
                ThemDinh();
            }
            else
            {
                MessageBox.Show("Bạn phải chọn file XML trước khi thực hiện thao tác");
            }
        }
        //Sửa đỉnh
        private void btnSuaD_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFilePath.Text))
            {
                SuaDinh();
            }
            else
            {
                MessageBox.Show("Bạn phải chọn file XML trước khi thực hiện thao tác");
            }
        }
        //Xóa đỉnh
        private void btnXoaD_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFilePath.Text))
            {
                XoaDinh();
            }
            else
            {
                MessageBox.Show("Bạn phải chọn file XML trước khi thực hiện thao tác");
            }
        }
        //Tính toán tuần tự
        private void btnTT_Click(object sender, EventArgs e)
        {
            clearT();
            //GetData();
            GetData();
            //Đo thời gian chạy
            Stopwatch sw = new Stopwatch();
            sw.Start();
            //Thực hiện tính tuần tự
            foreach (var Dothi in CacDothi)
            {
                ToMau(Dothi);
            }
            sw.Stop();
            string RunTime = sw.ElapsedMilliseconds + "ms";
            lbTime.Text = "Thời gian thực hiện: " + RunTime;
            lbResult.Items.Add("Thời gian tính toán tuần tự: " + RunTime);
        }
        //Tính toán song song
        private void btnSS_Click(object sender, EventArgs e)
        {
            clearT();
            GetData();
            //Tạo thread
            Thread thread2 = new Thread(Tomau2);
            Thread thread3 = new Thread(Tomau3);
            Thread thread4 = new Thread(Tomau4);
            //Đặt tên
            if(string.IsNullOrEmpty(Thread.CurrentThread.Name))
            {
                Thread.CurrentThread.Name = "Thread 1";
            }
            thread2.Name = "Thread 2";
            thread3.Name = "Thread 3";
            thread4.Name = "Thread 4";
            //Tính toán song song
            Stopwatch sw = new Stopwatch();
            sw.Start();
            thread2.Start();
            thread3.Start();
            thread4.Start();
            Tomau1();         
            sw.Stop();
            string RunTime = sw.ElapsedMilliseconds + "ms";
            lbTime.Text = "Thời gian thực hiện: " + RunTime;
            //lbResult.Items.Add("Thời gian tính toán song song: " + RunTime);
        }
        //Chia nhỏ tập các đồ thị
        void Tomau1()
        {
            for(int i = 0; i < sl; i++)
            {
                ToMau(CacDothi[i]);
            }
        }
        void Tomau2()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = sl; i <2* sl; i++)
            {
                ToMau(CacDothi[i]);
            }
            //Kết thúc thread
            Thread.CurrentThread.Abort();
            sw.Stop();
            string RunTime = sw.ElapsedMilliseconds + "ms";
            lbTime.Text = "Thời gian thực hiện: " + RunTime;
        }
        void Tomau3()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 2*sl; i < 3 * sl; i++)
            {
                ToMau(CacDothi[i]);
            }
            //Kết thúc thread
            Thread.CurrentThread.Abort();
            sw.Stop();
            string RunTime = sw.ElapsedMilliseconds + "ms";
            lbTime.Text = "Thời gian thực hiện: " + RunTime;
        }
        void Tomau4()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 3 * sl; i < sldt; i++)
            {
                ToMau(CacDothi[i]);
            }
            //Kết thúc thread
            Thread.CurrentThread.Abort();
            sw.Stop();
            string RunTime = sw.ElapsedMilliseconds + "ms";
            lbTime.Text = "Thời gian thực hiện: " + RunTime;
        }
        //Thuật toán tô màu đồ thị 
        void ToMau(DoThi Dothi)
        {
            List<Color> colors = new List<Color>();
            //Sắp xếp các đỉnh theo thứ tự bậc đỉnh giảm dần
            var CacDinh = Dothi.CacDinh.OrderByDescending(p => p.BacDinh).ToList();
            int i = 0;
            while (CacDinh.Count() > 0)
            {
                //Lấy đỉnh có bậc đỉnh cao nhất
                var Dinh = CacDinh.FirstOrDefault();
                //Tạo màu mới
                Color color = new Color();
                color.ColorName = (i + 1).ToString();
                List<string> ColorDinh = new List<string>();
                //Tô màu cho đỉnh
                ColorDinh.Add(Dinh.TenDinh);
                //Duyệt các đỉnh khác
                foreach (var D in CacDinh)
                {
                    if (!Dinh.CacDinhKe.Contains(D.TenDinh)&&D.TenDinh!=Dinh.TenDinh)
                    {
                        //Tô màu cho đỉnh
                        ColorDinh.Add(D.TenDinh);
                    }
                }
                //Loại bỏ đỉnh vừa tô màu
                foreach(var TD in ColorDinh)
                {
                    var D = CacDinh.Where(p => p.TenDinh == TD).FirstOrDefault();
                    if(D != null)
                    {
                        CacDinh.Remove(D);
                    }
                }
                color.TenDinh = ColorDinh;
                colors.Add(color);
                i++;
            }
            string ToMau = "Đồ thị" + Dothi.TenDoThi;
            foreach (var cl in colors)
            {
                string temp = "\tMàu " + cl.ColorName + " - đỉnh: ";
                foreach (var Dinh in cl.TenDinh)
                {
                    temp = temp + Dinh + ", ";
                }
                ToMau = ToMau + temp;
            }
            lbResult.Items.Add(ToMau);
            lbResult.Items.Add("\n-------------------------------------------------------------------------------------------------------------------------------\n");
        }
        //Load cbx
        void LoadCbx()
        {
            DataSet dts = new DataSet();
            dts.ReadXml(txtFilePath.Text);
            cbxTenDT1.DataSource = dts.Tables["DoThi"];
            cbxTenDT1.DisplayMember = "TenDT";
        }
        //Get Data
        void GetData()
        {
            if (!string.IsNullOrEmpty(txtFilePath.Text))
            {
                foreach (XElement xDothi in xdoc.Elements("DoThi"))
                {
                    DoThi DoThi = new DoThi();
                    string TenDT = xDothi.Attribute("TenDT").Value;

                    DoThi.TenDoThi = TenDT;
                    List<Dinh> ListDinh = new List<Dinh>();

                    foreach (XElement xDinh in xDothi.Elements("Dinh"))
                    {
                        Dinh Dinh = new Dinh();
                        string TenDinh = xDinh.Attribute("TenDinh").Value;
                        int bac = xDinh.Elements("DinhKe").Count();
                        Dinh.TenDinh = TenDinh;
                        Dinh.BacDinh = bac;
                        //string[] DK = new string[bac];
                        //int i = 0;
                        List<string> DK = new List<string>();
                        foreach (XElement xDinhKe in xDinh.Elements("DinhKe"))
                        {
                            //DK[i] = Convert.ToString(xDinhKe.Value);
                            //i++;
                            DK.Add(Convert.ToString(xDinhKe.Value));
                        }
                        Dinh.CacDinhKe = DK;
                        ListDinh.Add(Dinh);
                    }
                    DoThi.CacDinh = ListDinh;
                    CacDothi.Add(DoThi);
                }
                //Chia cá đồ thị để chia vào các luồng
                sldt = CacDothi.Count;
                sl = sldt / 4;
            }
            else
            {
                MessageBox.Show("Bạn phải chọn file XML trước khi thực hiện thao tác");
            }
        }
        //Hiện thị dữ liệu từ file XML
        void ShowData()
        {
            lbData.Items.Clear();
            lbData.Items.Add("Danh sách các đồ thị:");

            xdoc = XElement.Load(txtFilePath.Text);

            foreach (XElement xDothi in xdoc.Elements("DoThi"))
            {
                string TenDT = xDothi.Attribute("TenDT").Value;
                lbData.Items.Add("- Đồ thị: " + TenDT);
                lbData.Items.Add("\tĐỉnh \tBậc \tĐỉnh kề");
                foreach (XElement xDinh in xDothi.Elements("Dinh"))
                {
                    string TenDinh = xDinh.Attribute("TenDinh").Value;
                    int bac = xDinh.Elements("DinhKe").Count();
                    string DinhKe = "";
                    foreach (XElement xDinhKe in xDinh.Elements("DinhKe"))
                    {
                        DinhKe = DinhKe + xDinhKe.Value + ", ";
                    }
                    lbData.Items.Add("\t" + TenDinh + "\t " + bac + "\t " + DinhKe);
                }
                lbData.Items.Add("\n-------------------------------------------------------------------------------------------------------------------------------\n");
            }
        }
        //Hiện thị thông tin Đồ thị
        void ShowDT(string TenDT)
        {
            lbData.Items.Clear();
            lbData.Items.Add("Đồ thị: "+TenDT);
            foreach (XElement xDothi in xdoc.Elements("DoThi"))
            {
                if (xDothi.Attribute("TenDT").Value == TenDT)
                {
                    lbData.Items.Add("\tĐỉnh \tBậc \tĐỉnh kề");
                    foreach (XElement xDinh in xDothi.Elements("Dinh"))
                    {
                        string TenDinh = xDinh.Attribute("TenDinh").Value;
                        int bac = xDinh.Elements("DinhKe").Count();
                        string DinhKe = "";
                        foreach (XElement xDinhKe in xDinh.Elements("DinhKe"))
                        {
                            DinhKe = DinhKe + xDinhKe.Value + ", ";
                        }
                        lbData.Items.Add("\t" + TenDinh + "\t " + bac + "\t " + DinhKe);
                    }
                }
            }
        }
        //Test
        void TestGetData()
        {
            GetData();
            foreach (DoThi Dothi in CacDothi)
            {
                lbResult.Items.Add("Tên đồ thị:" + Dothi.TenDoThi);
                foreach (Dinh Dinh in Dothi.CacDinh)
                {
                    lbResult.Items.Add("\tTên đỉnh" + Dinh.TenDinh);
                    lbResult.Items.Add("\t\tBậc đỉnh" + Dinh.BacDinh);
                    string DK = "";
                    foreach (string DinhKe in Dinh.CacDinhKe)
                    {
                        DK = DK + DinhKe + ", ";
                    }
                    lbResult.Items.Add("\t\tCác đỉnh kề:" + DK);
                }
            }
        }
        //Thêm Đồ thị
        void ThemDT()
        {
            List<XElement> d = xdoc.Elements("DoThi").Where(s => s.Attribute("TenDT").Value == cbxTenDT1.Text).ToList();
            if (d.Count == 0)
            {
                XElement DoThi = new XElement("DoThi");
                DoThi.Add(new XAttribute("TenDT", cbxTenDT1.Text));
                xdoc.Add(DoThi);
                xdoc.Save(txtFilePath.Text);
                ShowDT(cbxTenDT1.Text);
            }
            else
            {
                MessageBox.Show("Tên đồ thị đã tồn tại");
            }
        }
        //Xóa Đồ thị
        void XoaDT()
        {
            xdoc.Descendants("DoThi").Where(s => s.Attribute("TenDT").Value == cbxTenDT1.Text).Remove();
            xdoc.Save(txtFilePath.Text);
            ShowData();
        }
        //Thêm đỉnh
        void ThemDinh()
        {
            
            XElement DoThi = xdoc.Elements("DoThi").Where(s => s.Attribute("TenDT").Value == cbxTenDT1.Text).Single();
            List<XElement> d = DoThi.Elements("Dinh").Where(s => s.Attribute("TenDinh").Value == txtTenDinh.Text.Trim()).ToList();
            if (d.Count==0)
            {
                XElement Dinh = new XElement("Dinh");
                Dinh.Add(new XAttribute("TenDinh", txtTenDinh.Text));
                string[] DK = txtDinhKe.Text.Split(',');
                //Thêm đỉnh kề
                foreach (string s in DK)
                {
                    Dinh.Add(new XElement("DinhKe", s.Trim()));
                }
                //Thêm đỉnh vào đồ thị
                xdoc.Elements("DoThi").Where(s => s.Attribute("TenDT").Value == cbxTenDT1.Text).Single().Add(Dinh);
                xdoc.Save(txtFilePath.Text);
                ShowDT(cbxTenDT1.Text);
            }
            else
            {
                MessageBox.Show("Tên đỉnh đã tồn tại");
            }
        }
        //Xóa đỉnh
        void XoaDinh()
        {
            var DoThi = from s in xdoc.Descendants("DoThi") where s.Attribute("TenDT").Value == cbxTenDT1.Text select s;
            DoThi.Elements("Dinh").Where(s => s.Attribute("TenDinh").Value == txtTenDinh.Text.Trim()).Remove();
            // xdoc.Descendants("DoThi").Where(s => s.Attribute("TenDT").Value == cbxTenDT1.Text).Elements("Dinh").Where(s => s.Attribute("TenDinh").Value == txtTenDinh.Text).Remove();
            xdoc.Save(txtFilePath.Text);
            ShowDT(cbxTenDT1.Text);
        }
        //Sửa đỉnh
        void SuaDinh()
        {
            var DoThi = from s in xdoc.Descendants("DoThi") where s.Attribute("TenDT").Value == cbxTenDT1.Text select s;
            var Dinh = DoThi.Elements("Dinh").Where(s => s.Attribute("TenDinh").Value == txtTenDinh.Text.Trim()).Single();
            Dinh.Elements("DinhKe").Remove();
            string[] DK = txtDinhKe.Text.Split(',');
            //Thêm đỉnh kề
            foreach (string s in DK)
            {
                Dinh.Add(new XElement("DinhKe", s.Trim()));
            }
            xdoc.Save(txtFilePath.Text);
            ShowDT(cbxTenDT1.Text);
        }
        //Clear
        void clearT()
        {
                lbResult.Items.Clear();          
                lbTime.Text = "Thời gian thực hiện:";
                CacDothi.Clear();
        }
        void clear()
        {
            txtFilePath.Text = "";
            lbData.Items.Clear();
            cbxTenDT1.Text = "";
            txtTenDinh.Text = "";
            txtDinhKe.Text = "";
            clearT();
        }
    }
}
