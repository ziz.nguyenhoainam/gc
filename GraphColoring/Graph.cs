﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphColoring
{
    public class DoThi
    {
        public string TenDoThi { get; set; }
        public List<Dinh> CacDinh { get; set; }
    }
    public class Dinh
    {
        public string TenDinh { get; set; }
        public int BacDinh { get; set; }
        //public string[] CacDinhKe { get; set; }
        public List<string> CacDinhKe { get; set; }
    }
    public class Color
    {
        public string ColorName { get; set; }
        public List<String> TenDinh { get; set; }
    }
}
