﻿namespace GraphColoring
{
    partial class GraphColoring
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialogXML = new System.Windows.Forms.OpenFileDialog();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnThemD = new System.Windows.Forms.Button();
            this.btnSuaD = new System.Windows.Forms.Button();
            this.btnXoaD = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnShowDT = new System.Windows.Forms.Button();
            this.cbxTenDT1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnXoaDT = new System.Windows.Forms.Button();
            this.btnThemDT = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtDinhKe = new System.Windows.Forms.TextBox();
            this.txtTenDinh = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lbTime = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbResult = new System.Windows.Forms.ListBox();
            this.lbData = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnTT = new System.Windows.Forms.Button();
            this.btnSS = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialogXML
            // 
            this.openFileDialogXML.FileName = "openFileDialogXML";
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(871, 589);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 33);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnThemD
            // 
            this.btnThemD.Location = new System.Drawing.Point(66, 284);
            this.btnThemD.Name = "btnThemD";
            this.btnThemD.Size = new System.Drawing.Size(75, 33);
            this.btnThemD.TabIndex = 11;
            this.btnThemD.Text = "Thêm";
            this.btnThemD.UseVisualStyleBackColor = true;
            this.btnThemD.Click += new System.EventHandler(this.btnThemD_Click);
            // 
            // btnSuaD
            // 
            this.btnSuaD.Location = new System.Drawing.Point(169, 284);
            this.btnSuaD.Name = "btnSuaD";
            this.btnSuaD.Size = new System.Drawing.Size(75, 33);
            this.btnSuaD.TabIndex = 12;
            this.btnSuaD.Text = "Sửa";
            this.btnSuaD.UseVisualStyleBackColor = true;
            this.btnSuaD.Click += new System.EventHandler(this.btnSuaD_Click);
            // 
            // btnXoaD
            // 
            this.btnXoaD.Location = new System.Drawing.Point(270, 284);
            this.btnXoaD.Name = "btnXoaD";
            this.btnXoaD.Size = new System.Drawing.Size(75, 33);
            this.btnXoaD.TabIndex = 13;
            this.btnXoaD.Text = "Xóa";
            this.btnXoaD.UseVisualStyleBackColor = true;
            this.btnXoaD.Click += new System.EventHandler(this.btnXoaD_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnShowDT);
            this.groupBox1.Controls.Add(this.cbxTenDT1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnXoaDT);
            this.groupBox1.Controls.Add(this.btnThemDT);
            this.groupBox1.Location = new System.Drawing.Point(599, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(375, 190);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Đồ thị";
            // 
            // btnShowDT
            // 
            this.btnShowDT.Location = new System.Drawing.Point(66, 123);
            this.btnShowDT.Name = "btnShowDT";
            this.btnShowDT.Size = new System.Drawing.Size(75, 32);
            this.btnShowDT.TabIndex = 17;
            this.btnShowDT.Text = "Xem";
            this.btnShowDT.UseVisualStyleBackColor = true;
            this.btnShowDT.Click += new System.EventHandler(this.btnShowDT_Click);
            // 
            // cbxTenDT1
            // 
            this.cbxTenDT1.FormattingEnabled = true;
            this.cbxTenDT1.Location = new System.Drawing.Point(142, 41);
            this.cbxTenDT1.Name = "cbxTenDT1";
            this.cbxTenDT1.Size = new System.Drawing.Size(203, 23);
            this.cbxTenDT1.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 15);
            this.label2.TabIndex = 15;
            this.label2.Text = "Tên đồ thị";
            // 
            // btnXoaDT
            // 
            this.btnXoaDT.Location = new System.Drawing.Point(273, 123);
            this.btnXoaDT.Name = "btnXoaDT";
            this.btnXoaDT.Size = new System.Drawing.Size(75, 33);
            this.btnXoaDT.TabIndex = 13;
            this.btnXoaDT.Text = "Xóa";
            this.btnXoaDT.UseVisualStyleBackColor = true;
            this.btnXoaDT.Click += new System.EventHandler(this.btnXoaDT_Click);
            // 
            // btnThemDT
            // 
            this.btnThemDT.Location = new System.Drawing.Point(170, 123);
            this.btnThemDT.Name = "btnThemDT";
            this.btnThemDT.Size = new System.Drawing.Size(75, 33);
            this.btnThemDT.TabIndex = 11;
            this.btnThemDT.Text = "Thêm";
            this.btnThemDT.UseVisualStyleBackColor = true;
            this.btnThemDT.Click += new System.EventHandler(this.btnThemDT_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtDinhKe);
            this.groupBox2.Controls.Add(this.txtTenDinh);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.btnXoaD);
            this.groupBox2.Controls.Add(this.btnSuaD);
            this.groupBox2.Controls.Add(this.btnThemD);
            this.groupBox2.Location = new System.Drawing.Point(599, 226);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(375, 342);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Đỉnh";
            // 
            // txtDinhKe
            // 
            this.txtDinhKe.Location = new System.Drawing.Point(142, 82);
            this.txtDinhKe.Name = "txtDinhKe";
            this.txtDinhKe.Size = new System.Drawing.Size(203, 21);
            this.txtDinhKe.TabIndex = 14;
            // 
            // txtTenDinh
            // 
            this.txtTenDinh.Location = new System.Drawing.Point(142, 38);
            this.txtTenDinh.Name = "txtTenDinh";
            this.txtTenDinh.Size = new System.Drawing.Size(203, 21);
            this.txtTenDinh.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(66, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 15);
            this.label8.TabIndex = 15;
            this.label8.Text = "Đỉnh kề";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(66, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 15);
            this.label7.TabIndex = 15;
            this.label7.Text = "Tên đỉnh";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(770, 588);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 34);
            this.btnClear.TabIndex = 10;
            this.btnClear.Text = "Làm mới";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(107, 33);
            this.txtFilePath.Margin = new System.Windows.Forms.Padding(4);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(332, 21);
            this.txtFilePath.TabIndex = 0;
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenFile.Location = new System.Drawing.Point(447, 29);
            this.btnOpenFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(90, 30);
            this.btnOpenFile.TabIndex = 1;
            this.btnOpenFile.Text = "Open";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnInputFile_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 33);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Chọn file Xml";
            // 
            // lbTime
            // 
            this.lbTime.AutoSize = true;
            this.lbTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTime.Location = new System.Drawing.Point(18, 585);
            this.lbTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(122, 16);
            this.lbTime.TabIndex = 5;
            this.lbTime.Text = "Thời gian thực hiện:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 342);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Kết quả";
            // 
            // lbResult
            // 
            this.lbResult.FormattingEnabled = true;
            this.lbResult.ItemHeight = 15;
            this.lbResult.Location = new System.Drawing.Point(19, 372);
            this.lbResult.Margin = new System.Windows.Forms.Padding(4);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(518, 184);
            this.lbResult.TabIndex = 6;
            // 
            // lbData
            // 
            this.lbData.FormattingEnabled = true;
            this.lbData.ItemHeight = 15;
            this.lbData.Location = new System.Drawing.Point(20, 89);
            this.lbData.Margin = new System.Windows.Forms.Padding(4);
            this.lbData.Name = "lbData";
            this.lbData.Size = new System.Drawing.Size(518, 244);
            this.lbData.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 69);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "Thông tin";
            // 
            // btnTT
            // 
            this.btnTT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTT.Location = new System.Drawing.Point(249, 576);
            this.btnTT.Margin = new System.Windows.Forms.Padding(4);
            this.btnTT.Name = "btnTT";
            this.btnTT.Size = new System.Drawing.Size(140, 30);
            this.btnTT.TabIndex = 9;
            this.btnTT.Text = "Tính toán tuần tự";
            this.btnTT.UseVisualStyleBackColor = true;
            this.btnTT.Click += new System.EventHandler(this.btnTT_Click);
            // 
            // btnSS
            // 
            this.btnSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSS.Location = new System.Drawing.Point(397, 576);
            this.btnSS.Margin = new System.Windows.Forms.Padding(4);
            this.btnSS.Name = "btnSS";
            this.btnSS.Size = new System.Drawing.Size(140, 30);
            this.btnSS.TabIndex = 9;
            this.btnSS.Text = "Tính toán song song";
            this.btnSS.UseVisualStyleBackColor = true;
            this.btnSS.Click += new System.EventHandler(this.btnSS_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnSS);
            this.groupBox3.Controls.Add(this.btnTT);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.lbData);
            this.groupBox3.Controls.Add(this.lbResult);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.lbTime);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.btnOpenFile);
            this.groupBox3.Controls.Add(this.txtFilePath);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(566, 625);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tô màu đồ thị";
            // 
            // GraphColoring
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 649);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnClose);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(1028, 687);
            this.MinimumSize = new System.Drawing.Size(1028, 687);
            this.Name = "GraphColoring";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Graph Coloring";
            this.Load += new System.EventHandler(this.GraphColoring_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialogXML;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnThemD;
        private System.Windows.Forms.Button btnSuaD;
        private System.Windows.Forms.Button btnXoaD;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnXoaDT;
        private System.Windows.Forms.Button btnThemDT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDinhKe;
        private System.Windows.Forms.TextBox txtTenDinh;
        private System.Windows.Forms.ComboBox cbxTenDT1;
        private System.Windows.Forms.Button btnShowDT;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox lbResult;
        private System.Windows.Forms.ListBox lbData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnTT;
        private System.Windows.Forms.Button btnSS;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}

