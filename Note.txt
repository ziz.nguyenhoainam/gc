* Thực hiện gán màu cho mỗi đỉnh của đồ thị, sao cho 2 đỉnh kề nhau không có cùng 1 màu, số màu sử dụng là ít nhất.
	- Input G=(V, E ).
	- Output G= (V, E ) có các đỉnh được gán màu.
1. Tính giá trị bậc của các đỉnh trog V
	Lập danh sách V'[v1 v2 ...vn] ( các đỉnh của đồ thị được sắp xếp giảm dần d(v1)>d(v1)>...d(vn)).
	Ban đầu các đỉnh k dc tô màu.
Gán i=1;
2. Tô màu i cho đỉnh đầu tiên trong danh sách V'
	Duyệt lần lượt các đỉnh khác trong V' và chỉ tô màu cho các đỉnh không kề với đỉnh đã có màu i.
3. Kiểm tra tất cả các đỉnh trong V -> đã được tô màu -> kết thúc. Ngược lại:
4. Loại bỏ các đỉnh đã được tô màu khỏi V'. Sắp xếp, Gán i=i+1, Quay lại bước 2


Input dữ liệu bằng file XML
Dữ liệu:
Đồ thị
	Đỉnh ( tên, bậc)
		Các đỉnh kề (List)
Màu
	Tên Màu
	Tên các đỉnh được tô màu(List)
	
Dữ liệu được lưu trong file XML
Input file XML bằng Openfiledialog
Nhập vào dãy các đỉnh kề ngăn cách = dấu "," (1 chỗi kí tự)
Tách chuỗi lưu vào list( hoặc mảng) sau đó đếm để tính bậc
Tạo 4 list  Bậc đỉnh (lưu bậc của các đỉnh trong đồ thị)
			Tên đỉnh (lưu tên của các đỉnh trong đồ thị)
			Đỉnh kề	(Lưu tên các đỉnh kề của đỉnh có bậc cao nhất trong mỗi lần duyệt)
			Màu ( lưu tên màu và tên các đỉnh được tô màu đó)
			
B1: Gán Tên đỉnh, Bậc đỉnh vào list tương ứng
	Tìm max trong list Bậc đỉnh
	tìm index của giã trị max vừa tìm được trong Bậc đỉnh
	từ index vừa tìm lấy ra Tên đỉnh có bậc cao nhất
	Từ tên đỉnh lấy ra các đỉnh kề và lưu vào List Đỉnh kề
	Gán màu i=1
B2: Tô màu i cho đỉnh vừa tìm được (Thêm tên đỉnh vào list Màu với tên màu = i), loại bỏ tên và bậc của đỉnh đó khỏi list tương ứng
	Duyệt mảng tên đỉnh:
		Nếu tên đỉnh không có trong list Đỉnh kề thì tô màu i cho đỉnh đó, loại bỏ tên và bậc của đỉnh đó khỏi list tương ứng
B3	Nếu List tên đỉnh không còn phần tử nào thì dừng lại
	Không thì màu i=i+1 và tiếp tực quay lại bước 2